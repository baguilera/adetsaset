package py.com.adetsaset.repository;

import java.util.List;
import java.util.Optional;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;
import org.springframework.stereotype.Repository;

import py.com.adetsaset.domain.AdetsaSetEntity;

@Repository
public interface AdetsaSetDAO extends JpaRepository<AdetsaSetEntity, Long> {

	@Query(value = "select t from AdetsaSetEntity t where t.amount >= :filterAmount order by t.fullName asc", nativeQuery = false)
	Optional<List<AdetsaSetEntity>> searchGreaterOrEqualTO(@Param("filterAmount") Long filterAmount);
	
	@Query(value = "select t from AdetsaSetEntity t where t.amount < :filterAmount order by t.fullName asc", nativeQuery = false)
	Optional<List<AdetsaSetEntity>> searchSmallerThan(@Param("filterAmount") Long filterAmount);
	
	@Query(value = "select t from AdetsaSetEntity t where t.peopleType = :peopleType order by t.fullName asc", nativeQuery = false)
	Optional<List<AdetsaSetEntity>> findByPeopleType(@Param("peopleType") String peopleType);
}
