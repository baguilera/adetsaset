package py.com.adetsaset;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.boot.autoconfigure.domain.EntityScan;
import org.springframework.context.annotation.ComponentScan;
import org.springframework.data.jpa.repository.config.EnableJpaRepositories;

@SpringBootApplication
@ComponentScan({"py.com.adetsaset"})
@EntityScan("py.com.adetsaset.domain")
@EnableJpaRepositories("py.com.adetsaset.repository")
public class AdetsasetApplication {

	public static void main(String[] args) {
		SpringApplication.run(AdetsasetApplication.class, args);
	}

}
