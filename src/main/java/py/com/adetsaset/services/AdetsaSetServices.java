package py.com.adetsaset.services;

import java.util.ArrayList;
import java.util.List;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import py.com.adetsaset.domain.AdetsaSetEntity;
import py.com.adetsaset.dto.Beneficiario;
import py.com.adetsaset.dto.Detalle;
import py.com.adetsaset.dto.Representante;
import py.com.adetsaset.dto.Retencion;
import py.com.adetsaset.dto.Root;
import py.com.adetsaset.dto.enums.BillType;
import py.com.adetsaset.dto.enums.InformadoPais;
import py.com.adetsaset.dto.enums.InformadoSituacion;
import py.com.adetsaset.dto.enums.InformadoTipoIdentificacion;
import py.com.adetsaset.dto.enums.PeopleType;
import py.com.adetsaset.dto.enums.RetencionMoneda;
import py.com.adetsaset.dto.enums.TransaccionTipoComprobante;
import py.com.adetsaset.exception.AdetsaApplicationException;
import py.com.adetsaset.repository.AdetsaSetDAO;
import py.com.adetsaset.util.DateHelper;

@Service
public class AdetsaSetServices {

	private static final Logger logger = LoggerFactory.getLogger(AdetsaSetServices.class);

	@Autowired
	private AdetsaSetDAO adetsaSetRepo;

	public List<Root> generateJsonByBillType(BillType billType) throws AdetsaApplicationException {
		try {
			List<Root> rootList = new ArrayList<>();
			Root root = new Root();

			List<AdetsaSetEntity> list = new ArrayList<>();
			if (BillType.BILL_WITHOUT_NAME.equals(billType)) {
				list.addAll(adetsaSetRepo.findByPeopleType(PeopleType.SOCIOS_INNOMINADOS.name())
						.orElse(new ArrayList<AdetsaSetEntity>()));
			} else if (BillType.BILL_WITH_NAME.equals(billType)) {
				list.addAll(adetsaSetRepo.findByPeopleType(PeopleType.CONTRIBUYENTE.name())
						.orElse(new ArrayList<AdetsaSetEntity>()));
				list.addAll(adetsaSetRepo.findByPeopleType(PeopleType.NO_CONTRIBUYENTE.name())
						.orElse(new ArrayList<AdetsaSetEntity>()));
			} else {
				throw new AdetsaApplicationException("El tipo de factura " + billType + " no se encuentra configurado");
			}

			Long sumAmount = 0l;
			for (AdetsaSetEntity entity : list) {
				root = new Root();
				root.getAtributos().setFechaCreacion(
						DateHelper.fromDate(entity.getWithholdingDate(), DateHelper.SIMPLE_DAY_FORMAT));
				root.getAtributos().setFechaHoraCreacion("");

				Detalle detalle = new Detalle();
				detalle.setCantidad(1);
				detalle.setTasaAplica(entity.getPercentageRate());
				detalle.setPrecioUnitario(entity.getAmount().intValue());
				detalle.setDescripcion(entity.getVoucherDescription());
				root.getDetalle().add(detalle);

				Retencion retencion = new Retencion();
				retencion.setMoneda(RetencionMoneda.PYG.name());
				retencion.setFecha(
						DateHelper.fromDate(entity.getWithholdingDate(), DateHelper.SIMPLE_DAY_FORMAT));
				retencion.setRetencionRenta(true);
				retencion.setConceptoRenta(entity.getWithholdingConceptIncome());

				retencion.setIvaPorcentaje5(0);
				retencion.setIvaPorcentaje10(0);
				retencion.setRentaCabezasBase(0);
				retencion.setRentaCabezasCantidad(0);
				retencion.setRentaToneladasBase(0);
				retencion.setRentaToneladasCantidad(0);
				retencion.setRentaPorcentaje(8);
				retencion.setRetencionIva(true);
				retencion.setConceptoIva("");
				root.setRetencion(retencion);

				root.getAtributos().setFechaCreacion(
						DateHelper.fromDate(entity.getWithholdingDate(), DateHelper.SIMPLE_DAY_FORMAT));
				if (entity.getDv() == null || entity.getDv().isEmpty()) {
					root.getInformado().setSituacion(InformadoSituacion.NO_CONTRIBUYENTE.name());
					root.getInformado().setRuc("");
					root.getInformado().setDv("");
					root.getInformado().setTipoIdentificacion(InformadoTipoIdentificacion.CEDULA.name());
					root.getInformado().setIdentificacion(entity.getDocNumber());
				} else {
					root.getInformado().setSituacion(InformadoSituacion.CONTRIBUYENTE.name());
					root.getInformado().setRuc(entity.getDocNumber());
					root.getInformado().setDv(entity.getDv());
					root.getInformado()
							.setTipoIdentificacion(InformadoTipoIdentificacion.IDENTIFICACION_TRIBUTARIA.name());
					root.getInformado().setIdentificacion(entity.getDocNumber().concat("-".concat(entity.getDv())));
				}
				root.getInformado().setNombre(entity.getFullName());
				root.getInformado().setCorreoElectronico(entity.getEmail());
				root.getInformado().setDireccion("");
				root.getInformado().setTelefono("");
				root.getInformado().setPais(InformadoPais.PARAGUAY.getValue());
				root.getInformado().setNombreFantasia(null);
				root.getInformado().setDomicilio("");
				root.getInformado().setTieneRepresentante(false);
				root.getInformado().setTieneBeneficiario(false);
				Representante representante = new Representante();
				representante.setTipoIdentificacion("");
				representante.setIdentificacion("");
				representante.setNombre("");
				root.getInformado().setRepresentante(null);
				Beneficiario beneficiario = new Beneficiario();
				beneficiario.setTipoIdentificacion("");
				beneficiario.setIdentificacion("");
				beneficiario.setNombre("");
				root.getInformado().setBeneficiario(null);

				root.getTransaccion().setNumeroComprobanteVenta(entity.getVoucherNumber());
				root.getTransaccion().setCondicionCompra(entity.getPurchaseCondition());
				TransaccionTipoComprobante documentType = TransaccionTipoComprobante.valueOf(entity.getDocumentType());
				root.getTransaccion().setTipoComprobante(documentType.getValue());
				root.getTransaccion().setFecha(
						DateHelper.fromDate(entity.getWithholdingDate(), DateHelper.SIMPLE_DAY_FORMAT));
				root.getTransaccion().setNumeroTimbrado(entity.getStamped());

				sumAmount = sumAmount + entity.getAmount();
				rootList.add(root);
			}

			logger.info("sumAmount: {}", sumAmount);
			return rootList;
		} catch (Exception e) {
			logger.error("#generateJsonFromSmallerThan - Exception: {}", e);
			throw new AdetsaApplicationException("Error generateJsonFromSmallerThan");
		}
	}
}
