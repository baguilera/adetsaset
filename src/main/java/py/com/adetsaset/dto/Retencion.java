package py.com.adetsaset.dto;

import com.fasterxml.jackson.annotation.JsonInclude;

@JsonInclude(JsonInclude.Include.NON_NULL)
public class Retencion {
	private String moneda;
	private String fecha;
	private Boolean retencionRenta;
	private String conceptoRenta;
	private Integer ivaPorcentaje5;
	private Integer ivaPorcentaje10;
	private Integer rentaCabezasBase;
	private Integer rentaCabezasCantidad;
	private Integer rentaToneladasBase;
	private Integer rentaToneladasCantidad;
	private Integer rentaPorcentaje;
	private Boolean retencionIva;
	private String conceptoIva;

	public String getMoneda() {
		return moneda;
	}

	public void setMoneda(String moneda) {
		this.moneda = moneda;
	}

	public String getFecha() {
		return fecha;
	}

	public void setFecha(String fecha) {
		this.fecha = fecha;
	}

	public Boolean getRetencionRenta() {
		return retencionRenta;
	}

	public void setRetencionRenta(Boolean retencionRenta) {
		this.retencionRenta = retencionRenta;
	}

	public String getConceptoRenta() {
		return conceptoRenta;
	}

	public void setConceptoRenta(String conceptoRenta) {
		this.conceptoRenta = conceptoRenta;
	}

	public Integer getIvaPorcentaje5() {
		return ivaPorcentaje5;
	}

	public void setIvaPorcentaje5(Integer ivaPorcentaje5) {
		this.ivaPorcentaje5 = ivaPorcentaje5;
	}

	public Integer getIvaPorcentaje10() {
		return ivaPorcentaje10;
	}

	public void setIvaPorcentaje10(Integer ivaPorcentaje10) {
		this.ivaPorcentaje10 = ivaPorcentaje10;
	}

	public Integer getRentaCabezasBase() {
		return rentaCabezasBase;
	}

	public void setRentaCabezasBase(Integer rentaCabezasBase) {
		this.rentaCabezasBase = rentaCabezasBase;
	}

	public Integer getRentaCabezasCantidad() {
		return rentaCabezasCantidad;
	}

	public void setRentaCabezasCantidad(Integer rentaCabezasCantidad) {
		this.rentaCabezasCantidad = rentaCabezasCantidad;
	}

	public Integer getRentaToneladasBase() {
		return rentaToneladasBase;
	}

	public void setRentaToneladasBase(Integer rentaToneladasBase) {
		this.rentaToneladasBase = rentaToneladasBase;
	}

	public Integer getRentaToneladasCantidad() {
		return rentaToneladasCantidad;
	}

	public void setRentaToneladasCantidad(Integer rentaToneladasCantidad) {
		this.rentaToneladasCantidad = rentaToneladasCantidad;
	}

	public Integer getRentaPorcentaje() {
		return rentaPorcentaje;
	}

	public void setRentaPorcentaje(Integer rentaPorcentaje) {
		this.rentaPorcentaje = rentaPorcentaje;
	}

	public Boolean getRetencionIva() {
		return retencionIva;
	}

	public void setRetencionIva(Boolean retencionIva) {
		this.retencionIva = retencionIva;
	}

	public String getConceptoIva() {
		return conceptoIva;
	}

	public void setConceptoIva(String conceptoIva) {
		this.conceptoIva = conceptoIva;
	}

	@Override
	public String toString() {
		return "Retencion [moneda=" + moneda + ", fecha=" + fecha + ", retencionRenta=" + retencionRenta
				+ ", conceptoRenta=" + conceptoRenta + ", ivaPorcentaje5=" + ivaPorcentaje5 + ", ivaPorcentaje10="
				+ ivaPorcentaje10 + ", rentaCabezasBase=" + rentaCabezasBase + ", rentaCabezasCantidad="
				+ rentaCabezasCantidad + ", rentaToneladasBase=" + rentaToneladasBase + ", rentaToneladasCantidad="
				+ rentaToneladasCantidad + ", rentaPorcentaje=" + rentaPorcentaje + ", retencionIva=" + retencionIva
				+ ", conceptoIva=" + conceptoIva + "]";
	}
}
