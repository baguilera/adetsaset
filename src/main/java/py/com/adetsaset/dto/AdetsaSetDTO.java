package py.com.adetsaset.dto;

import java.util.Date;
import java.util.HashMap;
import java.util.Map;

import com.fasterxml.jackson.annotation.JsonAnyGetter;
import com.fasterxml.jackson.annotation.JsonAnySetter;
import com.fasterxml.jackson.annotation.JsonIgnore;
import com.fasterxml.jackson.annotation.JsonProperty;

public class AdetsaSetDTO {

	@JsonProperty("id")
	public Long id;
	@JsonProperty("tipo_persona")
	private String peopleType;
	@JsonProperty("full_name")
	private String fullName;
	@JsonProperty("ci_ruc")
	private String docNumber;
	@JsonProperty("dv")
	private String dv;
	@JsonProperty("monto")
	private Long amount;
	@JsonProperty("correo")
	private String email;
	@JsonProperty("retencion_concepto_renta")
	private String withholdingConceptIncome;
	@JsonProperty("fecha_retencion")
	private Date withholdingDate;
	@JsonProperty("tipo_documento")
	private String documentType;
	@JsonProperty("nro_comprobante")
	private String voucherNumber;
	@JsonProperty("timbrado")
	private String stamped;
	@JsonProperty("condicion_compra")
	private String purchaseCondition;
	@JsonProperty("tasa")
	private String taxdescription;
	@JsonProperty("descripcion_comprobante")
	private String voucherDescription;
	@JsonProperty("porcentaje_retencion")
	private String percentageRate;
	@JsonProperty("estado")
	private String status;

	@JsonIgnore
	private Map<String, Object> additionalProperties = new HashMap<>();

	public Long getId() {
		return id;
	}

	public void setId(Long id) {
		this.id = id;
	}

	public String getPeopleType() {
		return peopleType;
	}

	public void setPeopleType(String peopleType) {
		this.peopleType = peopleType;
	}

	public String getFullName() {
		return fullName;
	}

	public void setFullName(String fullName) {
		this.fullName = fullName;
	}

	public String getDocNumber() {
		return docNumber;
	}

	public void setDocNumber(String docNumber) {
		this.docNumber = docNumber;
	}

	public String getDv() {
		return dv;
	}

	public void setDv(String dv) {
		this.dv = dv;
	}

	public Long getAmount() {
		return amount;
	}

	public void setAmount(Long amount) {
		this.amount = amount;
	}

	public String getEmail() {
		return email;
	}

	public void setEmail(String email) {
		this.email = email;
	}

	public String getWithholdingConceptIncome() {
		return withholdingConceptIncome;
	}

	public void setWithholdingConceptIncome(String withholdingConceptIncome) {
		this.withholdingConceptIncome = withholdingConceptIncome;
	}

	public Date getWithholdingDate() {
		return withholdingDate;
	}

	public void setWithholdingDate(Date withholdingDate) {
		this.withholdingDate = withholdingDate;
	}

	public String getDocumentType() {
		return documentType;
	}

	public void setDocumentType(String documentType) {
		this.documentType = documentType;
	}

	public String getVoucherNumber() {
		return voucherNumber;
	}

	public void setVoucherNumber(String voucherNumber) {
		this.voucherNumber = voucherNumber;
	}

	public String getStamped() {
		return stamped;
	}

	public void setStamped(String stamped) {
		this.stamped = stamped;
	}

	public String getPurchaseCondition() {
		return purchaseCondition;
	}

	public void setPurchaseCondition(String purchaseCondition) {
		this.purchaseCondition = purchaseCondition;
	}

	public String getTaxdescription() {
		return taxdescription;
	}

	public void setTaxdescription(String taxdescription) {
		this.taxdescription = taxdescription;
	}

	public String getVoucherDescription() {
		return voucherDescription;
	}

	public void setVoucherDescription(String voucherDescription) {
		this.voucherDescription = voucherDescription;
	}

	public String getPercentageRate() {
		return percentageRate;
	}

	public void setPercentageRate(String percentageRate) {
		this.percentageRate = percentageRate;
	}

	public String getStatus() {
		return status;
	}

	public void setStatus(String status) {
		this.status = status;
	}

	@JsonAnyGetter
	public Map<String, Object> getAdditionalProperties() {
		return this.additionalProperties;
	}

	@JsonAnySetter
	public void setAdditionalProperty(String name, Object value) {
		this.additionalProperties.put(name, value);
	}

	@Override
	public String toString() {
		return "AdetsaSetDTO [id=" + id + ", peopleType=" + peopleType + ", fullName=" + fullName + ", docNumber="
				+ docNumber + ", dv=" + dv + ", amount=" + amount + ", email=" + email + ", withholdingConceptIncome="
				+ withholdingConceptIncome + ", withholdingDate=" + withholdingDate + ", documentType=" + documentType
				+ ", voucherNumber=" + voucherNumber + ", stamped=" + stamped + ", purchaseCondition="
				+ purchaseCondition + ", taxdescription=" + taxdescription + ", voucherDescription="
				+ voucherDescription + ", percentageRate=" + percentageRate + ", status=" + status
				+ ", additionalProperties=" + additionalProperties + "]";
	}
}