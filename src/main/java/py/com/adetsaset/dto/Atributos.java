package py.com.adetsaset.dto;

import com.fasterxml.jackson.annotation.JsonInclude;
import com.fasterxml.jackson.annotation.JsonPropertyOrder;

@JsonInclude(JsonInclude.Include.NON_NULL)
@JsonPropertyOrder({ "fechaCreacion", "fechaHoraCreacion" })
public class Atributos {
	private String fechaCreacion;
	private String fechaHoraCreacion;

	public String getFechaCreacion() {
		return fechaCreacion;
	}

	public void setFechaCreacion(String fechaCreacion) {
		this.fechaCreacion = fechaCreacion;
	}

	public String getFechaHoraCreacion() {
		return fechaHoraCreacion;
	}

	public void setFechaHoraCreacion(String fechaHoraCreacion) {
		this.fechaHoraCreacion = fechaHoraCreacion;
	}

	@Override
	public String toString() {
		return "Atributos [fechaCreacion=" + fechaCreacion + ", fechaHoraCreacion=" + fechaHoraCreacion + "]";
	}
}