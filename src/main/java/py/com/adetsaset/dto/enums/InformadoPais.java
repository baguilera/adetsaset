package py.com.adetsaset.dto.enums;

public enum InformadoPais {
	PARAGUAY("PY"), ARGENTINA("AR"), BRASIL("BR");
	
	private String value;

	InformadoPais(String value) {
		this.value = value;
	}

	public static InformadoPais fromValue(String value) {
		if (value != null) {
			for (InformadoPais messageKey : values()) {
				if (messageKey.value.equals(value)) {
					return messageKey;
				}
			}
		}

		return getDefault();
	}

	public static InformadoPais getDefault() {
		return PARAGUAY;
	}

	public String getValue() {
		return value;
	}
}
