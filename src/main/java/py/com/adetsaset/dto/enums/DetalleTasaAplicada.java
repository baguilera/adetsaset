package py.com.adetsaset.dto.enums;

public enum DetalleTasaAplicada {
	CERO("0"), CINCO("5"), DIEZ("10");
	
	private String value;

	DetalleTasaAplicada(String value) {
		this.value = value;
	}

	public static DetalleTasaAplicada fromValue(String value) {
		if (value != null) {
			for (DetalleTasaAplicada messageKey : values()) {
				if (messageKey.value.equals(value)) {
					return messageKey;
				}
			}
		}

		return getDefault();
	}

	public static DetalleTasaAplicada getDefault() {
		return null;
	}

	public String getValue() {
		return value;
	}
}
