package py.com.adetsaset.dto.enums;

public enum TransaccionCondicionCompra {
	CREDITO, CONTADO;
}
