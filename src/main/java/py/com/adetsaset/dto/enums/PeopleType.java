package py.com.adetsaset.dto.enums;

public enum PeopleType {
	CONTRIBUYENTE, NO_CONTRIBUYENTE, SOCIOS_INNOMINADOS;
}
