package py.com.adetsaset.dto.enums;

public enum InformadoIdentificacion {
	NO_CONTRIBUYENTE_INNOMINADO_COOPERATIVA("44444402"), NO_CONTRIBUYENTE_INNOMINADOS_JUEGOS_DE_AZAR("44444403");

	private String value;

	InformadoIdentificacion(String value) {
		this.value = value;
	}

	public static InformadoIdentificacion fromValue(String value) {
		if (value != null) {
			for (InformadoIdentificacion messageKey : values()) {
				if (messageKey.value.equals(value)) {
					return messageKey;
				}
			}
		}

		return getDefault();
	}

	public static InformadoIdentificacion getDefault() {
		return null;
	}

	public String getValue() {
		return value;
	}
}
