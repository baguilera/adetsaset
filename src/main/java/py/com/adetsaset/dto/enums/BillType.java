package py.com.adetsaset.dto.enums;

public enum BillType {
	BILL_WITH_NAME("NOMINADO"), BILL_WITHOUT_NAME("INNOMINADO");
	
	private String value;

	BillType(String value) {
		this.value = value;
	}

	public static BillType fromValue(String value) {
		if (value != null) {
			for (BillType messageKey : values()) {
				if (messageKey.value.equals(value)) {
					return messageKey;
				}
			}
		}

		return getDefault();
	}

	public static BillType getDefault() {
		return null;
	}

	public String getValue() {
		return value;
	}
}
