package py.com.adetsaset.dto.enums;

public enum RetencionMoneda {
	EUR, PYG, USD, BRL;
}
