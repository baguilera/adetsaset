package py.com.adetsaset.dto.enums;

public enum InformadoSituacion {
	CONTRIBUYENTE, NO_CONTRIBUYENTE, NO_RESIDENTE
}
