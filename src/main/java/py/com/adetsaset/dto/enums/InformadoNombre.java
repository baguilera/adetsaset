package py.com.adetsaset.dto.enums;

public enum InformadoNombre {
	NO_CONTRIBUYENTE_INNOMINADO_COOPERATIVA("SOCIOS INNOMINADOS (COOPERATIVAS)"), 
	NO_CONTRIBUYENTE_INNOMINADOS_JUEGOS_DE_AZAR("BENEFICIARIOS INNOMINADOS (JUEGOS AZAR)");

	private String value;

	InformadoNombre(String value) {
		this.value = value;
	}

	public static InformadoNombre fromValue(String value) {
		if (value != null) {
			for (InformadoNombre messageKey : values()) {
				if (messageKey.value.equals(value)) {
					return messageKey;
				}
			}
		}
		return getDefault();
	}

	public static InformadoNombre getDefault() {
		return null;
	}

	public String getValue() {
		return value;
	}
}
