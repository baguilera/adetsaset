package py.com.adetsaset.dto.enums;

public enum TransaccionTipoComprobante {
	FACTURA(1), AUTO_FACTURA(5), ENTRADA_ESPECTACULOS_PUBLICOS(11), ESCRITURA_PUBLICA(17), OTROS(18), PLANILLA_PAGOS(
			19), LIQUIDACION_SALARIO(20);

	private Integer value;

	TransaccionTipoComprobante(Integer value) {
		this.value = value;
	}

	public static TransaccionTipoComprobante fromValue(Integer value) {
		if (value != null) {
			for (TransaccionTipoComprobante messageKey : values()) {
				if (messageKey.value.equals(value)) {
					return messageKey;
				}
			}
		}

		return getDefault();
	}

	public static TransaccionTipoComprobante getDefault() {
		return null;
	}

	public Integer getValue() {
		return value;
	}
}
