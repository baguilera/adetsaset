package py.com.adetsaset.dto;

import com.fasterxml.jackson.annotation.JsonInclude;
import com.fasterxml.jackson.annotation.JsonPropertyOrder;

@JsonInclude(JsonInclude.Include.NON_NULL)
@JsonPropertyOrder({ "cantidad", "tasaAplica", "precioUnitario", "descripcion" })
public class Detalle {
	private Integer cantidad;
	private String tasaAplica;
	private Integer precioUnitario;
	private String descripcion;

	public Integer getCantidad() {
		return cantidad;
	}

	public void setCantidad(Integer cantidad) {
		this.cantidad = cantidad;
	}

	public String getTasaAplica() {
		return tasaAplica;
	}

	public void setTasaAplica(String tasaAplica) {
		this.tasaAplica = tasaAplica;
	}

	public Integer getPrecioUnitario() {
		return precioUnitario;
	}

	public void setPrecioUnitario(Integer precioUnitario) {
		this.precioUnitario = precioUnitario;
	}

	public String getDescripcion() {
		return descripcion;
	}

	public void setDescripcion(String descripcion) {
		this.descripcion = descripcion;
	}

	@Override
	public String toString() {
		return "Detalle [cantidad=" + cantidad + ", tasaAplica=" + tasaAplica + ", precioUnitario=" + precioUnitario
				+ ", descripcion=" + descripcion + "]";
	}
}