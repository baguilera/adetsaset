package py.com.adetsaset.dto;

public class Representante {
	private String tipoIdentificacion;
	private String identificacion;
	private String nombre;

	public String getTipoIdentificacion() {
		return tipoIdentificacion;
	}

	public void setTipoIdentificacion(String tipoIdentificacion) {
		this.tipoIdentificacion = tipoIdentificacion;
	}

	public String getIdentificacion() {
		return identificacion;
	}

	public void setIdentificacion(String identificacion) {
		this.identificacion = identificacion;
	}

	public String getNombre() {
		return nombre;
	}

	public void setNombre(String nombre) {
		this.nombre = nombre;
	}

	@Override
	public String toString() {
		return "Representante [tipoIdentificacion=" + tipoIdentificacion + ", identificacion=" + identificacion
				+ ", nombre=" + nombre + "]";
	}
}
