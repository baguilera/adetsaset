package py.com.adetsaset.dto;

import java.util.ArrayList;
import java.util.List;

import com.fasterxml.jackson.annotation.JsonInclude;
import com.fasterxml.jackson.annotation.JsonPropertyOrder;

@JsonInclude(JsonInclude.Include.NON_NULL)
@JsonPropertyOrder({ "detalle", "retencion", "informado", "transaccion", "atributos"})
public class Root {
	private List<Detalle> detalle = new ArrayList<>();
	private Retencion retencion;
	private Informado informado;
	private Transaccion transaccion;
	private Atributos atributos;

	public Root() {
		initializer();
	}

	private void initializer() {
		this.detalle = new ArrayList<>();
		this.retencion = new Retencion();
		this.informado = new Informado();
		this.transaccion = new Transaccion();
		this.atributos = new Atributos();
	}

	public List<Detalle> getDetalle() {
		return detalle;
	}

	public void setDetalle(List<Detalle> detalle) {
		this.detalle = detalle;
	}

	public Retencion getRetencion() {
		return retencion;
	}

	public void setRetencion(Retencion retencion) {
		this.retencion = retencion;
	}

	public Informado getInformado() {
		return informado;
	}

	public void setInformado(Informado informado) {
		this.informado = informado;
	}

	public Transaccion getTransaccion() {
		return transaccion;
	}

	public void setTransaccion(Transaccion transaccion) {
		this.transaccion = transaccion;
	}

	public Atributos getAtributos() {
		return atributos;
	}

	public void setAtributos(Atributos atributos) {
		this.atributos = atributos;
	}

	@Override
	public String toString() {
		return "Root [detalle=" + detalle + ", retencion=" + retencion + ", informado=" + informado + ", transaccion="
				+ transaccion + ", atributos=" + atributos + "]";
	}
}
