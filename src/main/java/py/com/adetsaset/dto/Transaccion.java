package py.com.adetsaset.dto;

import com.fasterxml.jackson.annotation.JsonInclude;
import com.fasterxml.jackson.annotation.JsonPropertyOrder;

@JsonInclude(JsonInclude.Include.NON_NULL)
@JsonPropertyOrder({ "numeroComprobanteVenta", "condicionCompra", "tipoComprobante", "fecha", "numeroTimbrado" })
public class Transaccion {
	private String numeroComprobanteVenta;
	private String condicionCompra;
	private Integer tipoComprobante;
	private String fecha;
	private String numeroTimbrado;

	public String getNumeroComprobanteVenta() {
		return numeroComprobanteVenta;
	}

	public void setNumeroComprobanteVenta(String numeroComprobanteVenta) {
		this.numeroComprobanteVenta = numeroComprobanteVenta;
	}

	public String getCondicionCompra() {
		return condicionCompra;
	}

	public void setCondicionCompra(String condicionCompra) {
		this.condicionCompra = condicionCompra;
	}

	public Integer getTipoComprobante() {
		return tipoComprobante;
	}

	public void setTipoComprobante(Integer tipoComprobante) {
		this.tipoComprobante = tipoComprobante;
	}

	public String getFecha() {
		return fecha;
	}

	public void setFecha(String fecha) {
		this.fecha = fecha;
	}

	public String getNumeroTimbrado() {
		return numeroTimbrado;
	}

	public void setNumeroTimbrado(String numeroTimbrado) {
		this.numeroTimbrado = numeroTimbrado;
	}

	@Override
	public String toString() {
		return "Transaccion [numeroComprobanteVenta=" + numeroComprobanteVenta + ", condicionCompra=" + condicionCompra
				+ ", tipoComprobante=" + tipoComprobante + ", fecha=" + fecha + ", numeroTimbrado=" + numeroTimbrado
				+ "]";
	}
}
