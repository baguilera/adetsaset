package py.com.adetsaset.dto;

import com.fasterxml.jackson.annotation.JsonInclude;

@JsonInclude(JsonInclude.Include.NON_NULL)
public class Informado {
	private Object ruc;
	private Object dv;
	private String nombre;
	private String identificacion;
	private String situacion;
	private String tipoIdentificacion;
	private String correoElectronico;
	private String direccion;
	private String telefono;
	private String pais;
	private Object nombreFantasia;
	private String domicilio;
	private Boolean tieneRepresentante;
	private Boolean tieneBeneficiario;
	private Representante representante;
	private Beneficiario beneficiario;

	public Object getRuc() {
		return ruc;
	}

	public void setRuc(Object ruc) {
		this.ruc = ruc;
	}

	public Object getDv() {
		return dv;
	}

	public void setDv(Object dv) {
		this.dv = dv;
	}

	public String getNombre() {
		return nombre;
	}

	public void setNombre(String nombre) {
		this.nombre = nombre;
	}

	public String getIdentificacion() {
		return identificacion;
	}

	public void setIdentificacion(String identificacion) {
		this.identificacion = identificacion;
	}

	public String getSituacion() {
		return situacion;
	}

	public void setSituacion(String situacion) {
		this.situacion = situacion;
	}

	public String getTipoIdentificacion() {
		return tipoIdentificacion;
	}

	public void setTipoIdentificacion(String tipoIdentificacion) {
		this.tipoIdentificacion = tipoIdentificacion;
	}

	public String getCorreoElectronico() {
		return correoElectronico;
	}

	public void setCorreoElectronico(String correoElectronico) {
		this.correoElectronico = correoElectronico;
	}

	public String getDireccion() {
		return direccion;
	}

	public void setDireccion(String direccion) {
		this.direccion = direccion;
	}

	public String getTelefono() {
		return telefono;
	}

	public void setTelefono(String telefono) {
		this.telefono = telefono;
	}

	public String getPais() {
		return pais;
	}

	public void setPais(String pais) {
		this.pais = pais;
	}

	public Object getNombreFantasia() {
		return nombreFantasia;
	}

	public void setNombreFantasia(Object nombreFantasia) {
		this.nombreFantasia = nombreFantasia;
	}

	public String getDomicilio() {
		return domicilio;
	}

	public void setDomicilio(String domicilio) {
		this.domicilio = domicilio;
	}

	public Boolean getTieneRepresentante() {
		return tieneRepresentante;
	}

	public void setTieneRepresentante(Boolean tieneRepresentante) {
		this.tieneRepresentante = tieneRepresentante;
	}

	public Boolean getTieneBeneficiario() {
		return tieneBeneficiario;
	}

	public void setTieneBeneficiario(Boolean tieneBeneficiario) {
		this.tieneBeneficiario = tieneBeneficiario;
	}

	public Representante getRepresentante() {
		return representante;
	}

	public void setRepresentante(Representante representante) {
		this.representante = representante;
	}

	public Beneficiario getBeneficiario() {
		return beneficiario;
	}

	public void setBeneficiario(Beneficiario beneficiario) {
		this.beneficiario = beneficiario;
	}

	@Override
	public String toString() {
		return "Informado [ruc=" + ruc + ", dv=" + dv + ", nombre=" + nombre + ", identificacion=" + identificacion
				+ ", situacion=" + situacion + ", tipoIdentificacion=" + tipoIdentificacion + ", correoElectronico="
				+ correoElectronico + ", direccion=" + direccion + ", telefono=" + telefono + ", pais=" + pais
				+ ", nombreFantasia=" + nombreFantasia + ", domicilio=" + domicilio + ", tieneRepresentante="
				+ tieneRepresentante + ", tieneBeneficiario=" + tieneBeneficiario + ", representante=" + representante
				+ ", beneficiario=" + beneficiario + "]";
	}
}