package py.com.adetsaset.domain;

import java.util.Date;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.SequenceGenerator;
import javax.persistence.Table;

@Entity
@Table(name = "adetsa_set_idu", schema = "public")
public class AdetsaSetEntity {

	@Id
	@GeneratedValue(strategy = GenerationType.AUTO, generator = "adetsa_set_seq")
	@SequenceGenerator(name = "adetsa_set_seq", sequenceName = "public.adetsa_set_seq", schema = "public", initialValue = 1, allocationSize = 1)
	@Column(name = "id", nullable = false, unique = true)
	public Long id;
	@Column(name = "tipo_persona")
	private String peopleType;
	@Column(name = "full_name")
	private String fullName;
	@Column(name = "ci_ruc")
	private String docNumber;
	@Column(name = "dv")
	private String dv;
	@Column(name = "monto")
	private Long amount;
	@Column(name = "correo")
	private String email;
	@Column(name = "retencion_concepto_renta")
	private String withholdingConceptIncome;
	@Column(name = "fecha_retencion")
	private Date withholdingDate;
	@Column(name = "tipo_documento")
	private String documentType;
	@Column(name = "nro_comprobante")
	private String voucherNumber;
	@Column(name = "timbrado")
	private String stamped;
	@Column(name = "condicion_compra")
	private String purchaseCondition;
	@Column(name = "tasa")
	private String taxdescription;
	@Column(name = "descripcion_comprobante")
	private String voucherDescription;
	@Column(name = "porcentaje_retencion")
	private String percentageRate;
	@Column(name = "status")
	private String status;

	public Long getId() {
		return id;
	}

	public void setId(Long id) {
		this.id = id;
	}

	public String getPeopleType() {
		return peopleType;
	}

	public void setPeopleType(String peopleType) {
		this.peopleType = peopleType;
	}

	public String getFullName() {
		return fullName;
	}

	public void setFullName(String fullName) {
		this.fullName = fullName;
	}

	public String getDocNumber() {
		return docNumber;
	}

	public void setDocNumber(String docNumber) {
		this.docNumber = docNumber;
	}

	public String getDv() {
		return dv;
	}

	public void setDv(String dv) {
		this.dv = dv;
	}

	public Long getAmount() {
		return amount;
	}

	public void setAmount(Long amount) {
		this.amount = amount;
	}

	public String getEmail() {
		return email;
	}

	public void setEmail(String email) {
		this.email = email;
	}

	public String getWithholdingConceptIncome() {
		return withholdingConceptIncome;
	}

	public void setWithholdingConceptIncome(String withholdingConceptIncome) {
		this.withholdingConceptIncome = withholdingConceptIncome;
	}

	public Date getWithholdingDate() {
		return withholdingDate;
	}

	public void setWithholdingDate(Date withholdingDate) {
		this.withholdingDate = withholdingDate;
	}

	public String getDocumentType() {
		return documentType;
	}

	public void setDocumentType(String documentType) {
		this.documentType = documentType;
	}

	public String getVoucherNumber() {
		return voucherNumber;
	}

	public void setVoucherNumber(String voucherNumber) {
		this.voucherNumber = voucherNumber;
	}

	public String getStamped() {
		return stamped;
	}

	public void setStamped(String stamped) {
		this.stamped = stamped;
	}

	public String getPurchaseCondition() {
		return purchaseCondition;
	}

	public void setPurchaseCondition(String purchaseCondition) {
		this.purchaseCondition = purchaseCondition;
	}

	public String getTaxdescription() {
		return taxdescription;
	}

	public void setTaxdescription(String taxdescription) {
		this.taxdescription = taxdescription;
	}

	public String getVoucherDescription() {
		return voucherDescription;
	}

	public void setVoucherDescription(String voucherDescription) {
		this.voucherDescription = voucherDescription;
	}

	public String getPercentageRate() {
		return percentageRate;
	}

	public void setPercentageRate(String percentageRate) {
		this.percentageRate = percentageRate;
	}

	public String getStatus() {
		return status;
	}

	public void setStatus(String status) {
		this.status = status;
	}

	@Override
	public String toString() {
		return "AdetsaSetEntity [id=" + id + ", peopleType=" + peopleType + ", fullName=" + fullName + ", docNumber="
				+ docNumber + ", dv=" + dv + ", amount=" + amount + ", email=" + email + ", withholdingConceptIncome="
				+ withholdingConceptIncome + ", withholdingDate=" + withholdingDate + ", documentType=" + documentType
				+ ", voucherNumber=" + voucherNumber + ", stamped=" + stamped + ", purchaseCondition="
				+ purchaseCondition + ", taxdescription=" + taxdescription + ", voucherDescription="
				+ voucherDescription + ", percentageRate=" + percentageRate + ", status=" + status + "]";
	}
}