package py.com.adetsaset.validator;

import org.springframework.stereotype.Component;

import py.com.adetsaset.dto.enums.BillType;
import py.com.adetsaset.exception.AdetsaApplicationException;

@Component
public class ControllerRequestValidator {

	public void billTypeValidator(String billTypeString) throws AdetsaApplicationException {
		try {
			if (billTypeString == null) {
				throw new AdetsaApplicationException("Atributo bill-type no puede ser nulo");
			}

			if (BillType.fromValue(billTypeString) == null) {
				throw new AdetsaApplicationException("bill-type no configurado o no permitido");
			}

		} catch (AdetsaApplicationException e) {
			throw e;
		} catch (Exception e) {
			throw new AdetsaApplicationException("Error validando bill-type: Exception: " + e.getMessage());
		}
	}
}
