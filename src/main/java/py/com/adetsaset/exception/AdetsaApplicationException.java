package py.com.adetsaset.exception;

public class AdetsaApplicationException extends Exception {

	private static final long serialVersionUID = 5029508179517400869L;

	public AdetsaApplicationException(String message) {
		super(message);
	}

	public AdetsaApplicationException(Throwable e) {
		super(e);
	}
}
