package py.com.adetsaset.exception;

import java.util.Map;
import java.util.TreeMap;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpHeaders;
import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.ControllerAdvice;
import org.springframework.web.bind.annotation.ExceptionHandler;

import py.com.adetsaset.mapper.OrikaBeanMapper;
import py.com.adetsaset.response.AdetsaBaseResponse;

@ControllerAdvice
public class AdetsaSetExceptionHandler {
	private static final Logger logger = LoggerFactory.getLogger(AdetsaSetExceptionHandler.class);

	private static Map<String, HttpStatus> posibleStatus = new TreeMap<>();
	private static HttpHeaders responseHeaders = new HttpHeaders();
	
	@Autowired
	private OrikaBeanMapper mapper;
	
	private AdetsaSetExceptionHandler() {
		super();
	}

	static {
		posibleStatus.put(AdetsaInvocationException.TX_NOTFOUND, HttpStatus.NOT_FOUND);
		posibleStatus.put(AdetsaInvocationException.TX_INVALID_STATUS, HttpStatus.CONFLICT);
		posibleStatus.put(AdetsaInvocationException.TX_NO_FEES, HttpStatus.CONFLICT);
		posibleStatus.put(AdetsaInvocationException.TX_TIME_OUT, HttpStatus.REQUEST_TIMEOUT);
		posibleStatus.put(AdetsaInvocationException.TX_NOT_REVERSED_UNMODIFIED, HttpStatus.NOT_MODIFIED);
		posibleStatus.put(AdetsaInvocationException.TX_NO_INVOICE, HttpStatus.NOT_FOUND);

		responseHeaders.set(HttpHeaders.CONTENT_TYPE, MediaType.APPLICATION_JSON_VALUE);
	}
	
	@ExceptionHandler(AdetsaApplicationException.class)
	public ResponseEntity<AdetsaBaseResponse> handlerError(AdetsaApplicationException e) {
		logger.error("#handlerError - MibiTxException: {}", e.getMessage());
		AdetsaBaseResponse response = mapper.map(e, AdetsaBaseResponse.class);
		HttpStatus status = HttpStatus.CONFLICT;
		String errorCode = e.getMessage(); //e.getErrorCode();

		if (posibleStatus.containsKey(errorCode)) {
			status = posibleStatus.get(errorCode);
		} else {
			logger.warn("Unable to translate {}: {}", AdetsaApplicationException.class.getName(), e);
		}
		
		logger.error("#handlerError - Response: {}", response);
		return new ResponseEntity<>(response, responseHeaders, status);
	}
}
