package py.com.adetsaset;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.context.annotation.Profile;

import com.mangofactory.swagger.configuration.SpringSwaggerConfig;
import com.mangofactory.swagger.models.dto.ApiInfo;
import com.mangofactory.swagger.plugin.EnableSwagger;
import com.mangofactory.swagger.plugin.SwaggerSpringMvcPlugin;

//@Configuration
//@EnableSwagger
//@Profile(value={"default"})
public class SwaggerConfig {

    private SpringSwaggerConfig config;

    @Autowired
    public void setConfig(SpringSwaggerConfig config) {
        this.config = config;
    }

    @Bean
    public SwaggerSpringMvcPlugin getSwagger() {
        return new SwaggerSpringMvcPlugin(this.config).apiInfo(apiInfo())
        		.enable(true)
                .useDefaultResponseMessages(false)
                .includePatterns("/adetsaset/.*");
    }
    
    private ApiInfo apiInfo() {
        return new ApiInfo("ADETSA SET", "API for ADETSASET",
                "ADETSASET API terms of service", "adetsa@toyotoshi.com.py",
                "ADETSASET API Licence Type", "ADETSASET API License URL");
    }
}