package py.com.adetsaset.controller;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

import com.wordnik.swagger.annotations.ApiOperation;
import com.wordnik.swagger.annotations.ApiParam;

import py.com.adetsaset.exception.AdetsaApplicationException;
import py.com.adetsaset.util.JSONUtils;

@RestController
public class InitController {

	private static final Logger logger = LoggerFactory.getLogger(InitController.class);

	@ApiOperation(value = "Webservices de testeo", notes = "Webservices de testeo")
	@GetMapping(value = "/init", produces = MediaType.APPLICATION_JSON_VALUE)
	public ResponseEntity<String> init(
			@ApiParam(value = "valor importe busqueda de datos") @RequestParam(value = "smaller-than", required = false, defaultValue = "1000000") Long smallerThan)
			throws AdetsaApplicationException {
		try {
			logger.info("#init - REQUEST - smallerThan: {}", smallerThan);

			String response = "hola mundo";

			logger.info("#init - Response: {}", JSONUtils.toJSON(response));
			return new ResponseEntity<>(response, HttpStatus.OK);
		} catch (AdetsaApplicationException e) {
			logger.error("#init - AdetsaApplicationException: {}", e);
			throw e;
		} catch (Exception e) {
			logger.error("#init - Exception: {}", e);
			throw new AdetsaApplicationException(e);
		}
	}
}
