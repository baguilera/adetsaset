package py.com.adetsaset.controller;

import java.util.List;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

import com.wordnik.swagger.annotations.ApiOperation;
import com.wordnik.swagger.annotations.ApiParam;

import py.com.adetsaset.dto.Root;
import py.com.adetsaset.dto.enums.BillType;
import py.com.adetsaset.exception.AdetsaApplicationException;
import py.com.adetsaset.services.AdetsaSetServices;
import py.com.adetsaset.validator.ControllerRequestValidator;

@RestController
@RequestMapping("/api")
public class AdetsaSetController {
	private static final Logger logger = LoggerFactory.getLogger(AdetsaSetController.class);

	@Autowired
	private AdetsaSetServices adetsaSetServices;

	@Autowired
	private ControllerRequestValidator controllerValidator;

	@ApiOperation(value = "Devuelve el json correspondiente a los agrupaso menor a 1 millon", notes = "Devuelve el json correspondiente a los agrupaso menor a 1 millon")
	@GetMapping(value = "/generate-json-by-bill-type", produces = MediaType.APPLICATION_JSON_VALUE)
	public ResponseEntity<List<Root>> generateJsonByBillType(
			@ApiParam(value = "valor importe busqueda de datos") @RequestParam(value = "bill-type", required = false, defaultValue = "OTROS") String billType)
			throws AdetsaApplicationException {
		try {
			logger.info("#generateJsonFromSmallerThan - REQUEST - billType: {}", billType);

			controllerValidator.billTypeValidator(billType);

			List<Root> response = adetsaSetServices.generateJsonByBillType(BillType.fromValue(billType));

			logger.info("#generateJsonFromGreaterOrEqualTO - Response - size: {}", response.size());
			return new ResponseEntity<>(response, HttpStatus.OK);
		} catch (AdetsaApplicationException e) {
			logger.error("#generateJsonByBillType - AdetsaApplicationException: {}", e);
			throw e;
		} catch (Exception e) {
			logger.error("#generateJsonByBillType - Exception: {}", e);
			throw new AdetsaApplicationException(e);
		}
	}
}
